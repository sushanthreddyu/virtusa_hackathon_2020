/*
 * Request Handlers
 *
 */

const student = require('#routes/student')
const test = require('#routes/test')

let handlers = {}

handlers.test = test

handlers.student = student
// * Ping Handler
handlers.ping = (data, callback) => {
	callback(200)
}

// Not Found handler
handlers.notFound = (data, callback) => {
	callback(404)
}

// * Export the module
module.exports = handlers
