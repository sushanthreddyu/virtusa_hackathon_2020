const _data = require('#lib/data')
const mysql = require('mysql')

const db_config = require('#lib/sql')

// Mysql db variable create
const mysqlConnection = mysql.createConnection(db_config)

// mysql db connection
mysqlConnection.connect(err => {
	if (!err) console.log('Connection Established Successfully')
	else console.log('Connection Failed!' + JSON.stringify(err, undefined, 2))
})

// Local hanlder
let handler = {}

handler.test = (data, callback) => {
	const acceptedMethods = ['POST', 'GET', 'PUT', 'DELETE']
	if (acceptedMethods.indexOf(data.method) > -1) {
		handler._test[data.method](data, callback)
	} else {
		callback(405)
	}
}
handler._test = {}

handler._test.POST = (data, callback) => {
	const test = data.payload
	const query_str =
		'insert into tests values(null,?,?,?);select LAST_INSERT_ID();'
	mysqlConnection.query(
		query_str,
		[test.start_time, test.end_time, test.duration],
		(err, rows, fields) => {
			if (!err) {
				console.log(rows[1][0]['LAST_INSERT_ID()'])
				let test_id = rows[1][0]['LAST_INSERT_ID()']
				let path = '/data/tests/' + test_id
				_data.create_dir(path, err => {
					if (err) {
						callback(404, { err })
					}
				})
				let questions = test.questions
				Object.keys(questions).map((key, index) => {
					let path_input =
						'/data/tests/' + test_id + '/' + key + '/input'
					let abcd = _data.create_dir(path_input)
					console.log(abcd)

					if (abcd === true) {
						let inputs = questions[key].inputs
						Object.keys(inputs).map((key, index) => {
							let temp_path =
								'/tests/' + test_id + '/' + key + '/input'
							_data.create(temp_path, key, inputs[key], err => {
								if (err) {
									callback(404, { err })
								}
							})
						})
					} else {
						callback(404, { err: 'not making files' })
					}
					let path_output =
						'/data/tests/' + test_id + '/' + key + '/output'
					// console.log(questions)
					_data.create_dir(path_output, err => {
						if (err) {
							console.log(questions[key])
							callback(404, { err })
						} else {
							console.log('hello')

							let outputs = questions[key].outputs
							Object.keys(outputs).map((key, index) => {
								let temp_path =
									'/tests/' + test_id + '/' + key + '/output'
								console.log(outputs[key])

								_data.create(
									temp_path,
									key,
									outputs[key],
									err => {
										if (err) {
											callback(404, { err })
										}
									}
								)
							})
						}
					})
					let path_sub =
						'/data/tests/' + test_id + '/' + key + '/submissions'
					_data.create_dir(path_sub, err => {
						if (err) {
							callback(404, { err })
						}
					})
					// console.log(questions[key].info)
					let path_info = '/tests/' + test_id + '/' + key
					_data.create(
						path_info,
						'info',
						questions[key].info,
						err => {
							if (err) {
								callback(404, { err })
							}
						}
					)
				})
				callback(200)
			} else {
				callback(404, { err })
			}
		}
	)
}

module.exports = handler.test
