const bcrypt = require('bcrypt')
// const _data = require('#lib/data')
// const helpers = require('#lib/helpers')
const db_config = require('#lib/sql')
const mysql = require('mysql')

// Mysql db variable create
const mysqlConnection = mysql.createConnection(db_config)

// mysql db connection
mysqlConnection.connect(err => {
	if (!err) console.log('Connection Established Successfully')
	else console.log('Connection Failed!' + JSON.stringify(err, undefined, 2))
})

// Local Handler
let handler = {}

// * User Handler
handler.student = (data, callback) => {
	const acceptedMethods = ['POST', 'GET']
	if (acceptedMethods.indexOf(data.method) > -1) {
		handler._student[data.method](data, callback)
	} else {
		callback(405)
	}
}

// * Container for the user submethods
handler._student = {}

// student - POST
// Required data : firstName, LastName, phone, password, tosAgreement
// Optional data : none
handler._student.POST = (data, callback) => {
	const student = data.payload
	console.log(student)

	query_str =
		'insert into students(s_id,name,email,password,phno,section) values(null,?,?,?,?,?)'
	bcrypt.hash(student.password, 10, (hash_err, hash) => {
		if (!hash_err) {
			console.log(hash)
			mysqlConnection.query(
				query_str,
				[
					student.name,
					student.email,
					hash,
					student.phno,
					student.section,
				],
				(err, rows, fields) => {
					if (!err) {
						callback(200)
					} else {
						callback(404, { err })
					}
				}
			)
		} else {
			callback(404, { hash_err })
		}
	})
}

handler._student.GET = (data, callback) => {
	const student = data.queryStringObject
	query_str = 'select * from students where s_id = ?'
	mysqlConnection.query(query_str, student.id, (err, rows, fields) => {
		if (!err) {
			delete rows[0].password
			callback(200, rows[0])
		} else {
			callback(404, { err })
		}
	})
}

module.exports = handler.student
