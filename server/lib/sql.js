let config = {
	host: '127.0.0.1',
	user: 'edoc',
	password: 'edoc_password',
	database: 'code_eval_platform',
	multipleStatements: true,
	dateStrings: ['DATE', 'DATETIME'],
}
module.exports = config
